## Anwendungsfall (Use Case)

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112084338768562808</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/anwendungsfall</span>


> **tl/dr;** _(ca. 4 min Lesezeit): Anwendungsfälle stellen den Nutzen, den ein System für Benutzer*innen bereitstellt, in das Zentrum der Betrachtung. Mit ihnen können Anforderungen identifiziert und (tabellarisch oder als Diagramm) dokumentiert werden._

Dieser Artikel ist Teil einer Artikelreihe über Use-Cases: [Anwendungsfälle und deren tabellarische Erfassung](https://oer-informatik.de/anwendungsfall), [Grundlagen zu Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase) / [Tutorial zur Erstellung von UML-Anwendungsfalldiagrammen mit PlantUML](https://oer-informatik.de/uml-usecase-plantuml) / [Übungsaufgaben zu UML-Use-Case-Diagrammen](https://oer-informatik.de/uml-usecase-uebung).

### Was ist ein Anwendungsfall?

"A use case is all the ways of using a system to achieve a particular goal for a particular user. Taken together the set of all the use cases gives you all of the useful ways to use the system, and illustrates the value that it will provide." (Ivar Jacobson) ^[Ivar Jacobson, der Initiator der UseCase-Diagramme, in "Use-Case 2.0": https://www.ivarjacobson.com/sites/default/files/field_iji_file/article/use-case_2_0_jan11.pdf]

Ein Anwendungsfall (_Use Case_) ist eine abgeschlossene Aufgabe des beschriebenen Systems. Jeder Anwendungsfall wird durch einen Akteur ausgelöst. Anwendungsfälle müssen so geschnitten werden, dass sie selbst einen _eigenen Nutzen_ (ein messbares Ergebnis) produzieren.

Anwendungsfälle haben folgende Eigenschaften:

* Anwendungsfälle beschreiben den Nutzen, den ein Benutzer von einem System erwarten kann. Sie können nur funktionale Anforderungen beschreiben, Qualitätsanforderungen (nicht funktionale Anforderungen) müssen anderweitig dokumentiert werden.

* Sie sind für Anwender (Nutzer) in der Sprache der Problemdomäne und aus ihrer Perspektive geschrieben. Use-Cases enthalten somit keine Implementierungsdetails.

* Sie dienen zur Abstimmung der Systemgrenzen und somit des Projektumfangs.

Die Übersicht von Anwendungsfällen eines Systems wird häufig übersichtlich in einem [UML-Anwendungsfalldiagramm](https://oer-informatik.de/uml-usecase) dargestellt. Der Aufbau jedes einzelnen Anwendungsfalls wird gesondert formuliert - z.B. tabellarisch (wie unten dargestellt), oder als Flussdiagramm (z.B. EPK oder [UML-Aktivitätsdiagramm](https://oer-informatik.de/uml-aktivitaetsdiagramm)).

### Was ist ein Akteur in Anwendungsfällen?

Akteure stehen außerhalb des modellierten Systems und tauschen Informationen mit diesem aus. Es kann sich um menschliche Benutzer oder andere Systeme handeln. Oft werden diese über Rollen dargestellt, die die Eigenschaften gegenüber dem System beschreiben (z.B. Admin, Anwender, Besucher).

### Was sind Szenarien in Anwendungsfällen?

Szenarien präzisieren Anwendungsfälle:

* Sie beschreiben aus Nutzersicht, was Akteure in einem Anwendungsfall tun, sehen, erleben.

* Die für den Anwendungsfall relevanten Systemmerkmale werden konkret, fokussiert und informell beschreiben.

* Szenarien enthalten keine Fallunterscheidungen ("Wenn...dann..."), da jeder Weg durch das System ein eigenes Szenario darstellt.

### Wie erstelle ich einen Anwendungsfall?

1. Identifizierung der Akteure:

  * Wer nutzt das System direkt (=> Hauptakteure) oder wird vom System indirekt unterstützt (=> Nebenakteure)?

  * Auf wen ist das System angewiesen, um die Anforderungen erfüllen zu können (z.B. Administration) (=> Nebenakteure)

  * Können diese Akteure in Gruppen/Rollen zusammengefasst werden?

2. Identifizierung der Szenarien:

  * Welche Szenarien führen die oben identifizierten Akteure aus?

  * Zu welchen Anwendungsfällen können diese Szenarien zusammengefasst werden?

### Tabellarische Darstellung eines Anwendungsfalls

Einzelne Anwendungsfälle können strukturiert als Tabelle dargestellt werden. Wenn es für einen Anwendungsfall viele Nebenszenarien gibt empfiehlt es sich, diese an gesonderter Stelle zu dokumentieren.


|Name|Anzeige des Warenkorbs|
|--|:--|
|**Kurzbeschreibung**|Stellt alle zum Kauf reservierten Artikel dar|
|**Hauptakteur**<br/>löst den UseCase aus|Nutzer des Shops (angemeldeter Kunde oder Gast)|
|**Nebenakteur**<br/>wird beim UseCase <br>einbezogen|(Im konkreten Bespiel kein Nebenakteur, denkbar wäre im Bestellprozess z.B. ein Bezahldienstleister oder ein Versanddienstleister als Nebenakteur)|
|**Auslöser**<br/>Was macht der Hauptakteur,<br/> damit der Anwendungsfall<br/>getriggert wird?|Das Warenkorb-Symbol wird angeklickt|
|**Vorbedingung**<br/>Welche Voraussetzung müssen<br/> für einen fehlerfreien Ablauf<br/> gegeben sein?|Auswahl "gewerblich/privat" muss getroffen sein|
|**Nachbedingungen**<br/> kennzeichnen den<br/> Erfolgszustand<br/> / das Hauptszenario|Ausgabeseite mit der Liste aller reservierten Waren wird ausgegeben|
|**Fehlerzustand**<br/>kennzeichnen ein<br/> Nebenszenario|Dialoge, die weitere Daten erfordern (Land (VAT), Währung, privat/gewerblich) oder mit Infos zu aufgetretenen Problemen werden angezeigt |
|**Hauptszenario**|1. Der Nutzer klickt auf den Warenkorb<br/>2. das Pop-Up erscheint mit einer Zusammenfassung<br/>3. Der Nutzer klickt auf "Details anzeigen"<br/>4. Die Gesamtliste der erfassten reservierten Artikel wird ausgegeben<br/>5. die reservierte Menge, Preise, Nettosumme, Steuer, Bruttosumme und Versandkosten werden ausgegeben |
|**Nebenszenarien**<br/>die Abfolgen werden in<br/> gesonderten Listen erfasst -<br/> hier nur die Nennung|Leere Liste, Abfrage Währung, Abfrage privat/gewerblich, Cookies/Javascript nicht zugelassen, Artikel nicht mehr verfügbar, Preisänderung seit Bestellung, Artikel nicht gefunden |

Mithilfe des UML-Anwendungsfall-Diagramms lässt sich eine Übersicht aller Anwendungsfälle darstellen. Die Schritte einzelner Anwendungsfälle lassen sich beispielsweise mit dem UML-Aktivitätsdiagramm, dem UML-Sequenzdiagramm oder dem UML-Zustandsautomaten darstellen.

### Haupt- und Nebenszenarien und deren Nutzen im weiteren Verlauf

Es empfiehlt sich, die einzelnen Szenarien im Team gemeinsam durchzugehen, um so fehlende Abläufe zu identifizieren. Werden die Szenarien gut dokumentiert, so ist bereits ein guter Teil der Arbeit der Systemanalyse abgeschlossen.

Die Szenarien können beispielsweise genutzt werden, um per Nominalextraktion bei Datenbankmodellierung und **objektorientierter Analyse** zu helfen.

