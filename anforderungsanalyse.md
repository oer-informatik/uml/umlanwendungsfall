# Anforderungsanalyse (_requirement engineering_)

> **tl/dr;** _(ca. 11 min Lesezeit): Welche Techniken gibt es, um Anforderungen zu ermitteln, Dokumentieren, Gliedern, Validieren und Messbar zu machen? Welche Kriterien werden an gute fachgerechte und überprüfbare Anforderungen angelegt?_



Der Artikel befindet sich jetzt hier:

[https://oer-informatik.de/anforderungsanalyse](https://oer-informatik.de/anforderungsanalyse)

Bitte dort weiterlesen.

