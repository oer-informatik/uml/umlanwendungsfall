## UML Use-Case-Übungsaufgabe: Mitfahr-App

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/112084338768562808</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/uml-usecase-uebung</span>


> **tl/dr;** _(ca. 20 min Bearbeitungszeit mit Tool, mit Papier+Stift schneller): Übungsaufgabe zur Erstellung eines Use-Case-Diagramms anhand einer vorliegenden Beschreibung. Das Beispiel nutzt die Notationsvielfalt der UML, entsprechende Kenntnisse sind also vorausgesetzt._

Hier finden sich eine Reihe von Übungsaufgaben zu Use-Case-Diagrammen (mit ausgeblendeten Lösungen). Dieser Artikel ist Teil einer Artikelreihe über Use-Case-Diagramme: [Anwendungsfälle und deren tabellarische Erfassung](https://oer-informatik.de/anwendungsfall), [Grundlagen zum Anwendungsfall-Diagramm (Use Case)](https://oer-informatik.de/uml-usecase) / [Tutorial zur Erstellung von UML-Anwendungsfalldiagrammen mit PlantUML](https://oer-informatik.de/uml-usecase-plantuml) / [Übungsaufgaben zu UML-Use-Case-Diagrammen](https://oer-informatik.de/uml-usecase-uebung).

Wer die [Notationen des Use Case-Diagramms (Artikel)](https://oer-informatik.de/uml-usecase) kennt, kann sich an die folgenden Aufgaben wagen. 

### Anwendungsfalldiagramm für eine Prüfungsaufgaben-Such-App

Für einen Dienstleister, der Prüfungsvorbereitungs-Seminare anbietet, soll eine Web-App erstellt werden, mithilfe derer Übungsaufgaben aus alten IHK-Prüfungen gefunden werden können. Hierbei sollen nicht die Aufgaben selbst, sondern nur der Fundort der Aufgabe ausgegeben werden (z.B. "Teil 1 Prüfung IT-Berufe, Frühjahr 2024, 4. Aufgabe"). Die Inhalte der App werden von Moderierenden erstellt und von angemeldeten Benutzer*innen gelesen. 

Einzelne Prüfungen werden von Moderierenden mit Namen und Prüfungsart erfasst und bearbeitet (z.B. "Teil 2 Prüfung Fachinformatik/Anwendungsentwicklung, Frühjahr 2024").

Die jeweiligen Prüfungsthemen werden in einer Baumstruktur (Themenbaum) gegliedert, diese werden von Moderierenden erfasst und bearbeitet (z.B. "UML Sequenzdiagramm", "Relationenmodell erstellen", ...).

Aufgaben einer Prüfung (z.B. deren Stichwörter) werden von Moderierenden erfasst und bearbeitet. Das beinhaltet immer das Einhängen der Prüfung in den hierarchischen Themenbaum (ggf. an mehreren Stellen). (z.B. Aufgabe "2bb" der Prüfung "Teil 2 Prüfung Fachinformatik/Anwendungsentwicklung, Frühjahr 2023, 1. Planen eines Softwareproduktes" betrifft die Themen "Polymorphie" und "UML Klassendiagramm").

Angemeldete Benutzer*innen (dazu gehören auch Moderierende) sollen die Möglichkeit haben, sich über eine Suche nach Stichwörtern eine Liste mit Aufgaben ausgeben zu lassen (z.B. alle Aufgaben zu "Normalisierung"). 
Wenn angemeldete Benutzer*innen über den Themenbaum navigieren, wird Suchfunktion erweitert um die Filterung auf Einträge, die in der ausgewählten Struktur liegen. Habe ich beispielsweise im Themenbaum den Eintrag "UML" ausgewählt, so werden nur Suchergebnisse angezeigt, die "UML" betreffen. 

Wenn Benutzer*innen nicht angemeldet sind, werden Sie als Gast geführt. Gäste haben als einzigen Anwendungsfall die Möglichkeit, sich ein (kostenpflichtiges) Konto zu erstellen.

Für das Login ist keine Modellierung eines eigenständigen Anwendungsfalls erforderlich. 

Sofern Daten "erfasst und bearbeitet" werden, so soll "erfassen" und "bearbeiten" nicht als einzelne, sondern als ein gemeinsamer Anwendungsfall betrachtet werden. 

<button onclick="toggleAnswer('uc2')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="uc2">

Eine Beispiellösung könnte etwa so aussehen:

![UML-Use-Case-Diagramm zur Prüfungsaufgaben-Such-App](plantuml/uebungsaufgabensuche.png)

Hinweise, häufige Fehler:

- Ein häufiger Fehler ist es, den Usecase "Suche nach Aufgaben" mit Assoziationen zu zwei Akteuren (Angemeldete Benutzer*innen und Moderierende) zu versehen. So würden beide benötigt, um ausgeführt werden zu können. Das ist eine falsche ODER-Implementierung, korrekt muss diese mit Vererbung umgesetzt werden.

- Usecases haben keine einfachen Assoziationen untereinander und dürfen somit nicht mit Linien verbunden werden. Ausnahmen bilden include, extend und Vererbung.

- Die Assoziationen zwischen Usecases und Akteuren haben nur in Ausnahmen Pfeilspitzen. Üblicherweise nutzt man diese nur, wenn bei sekundären Akteuren die Navigationsrichtung relevant ist. (Pfeilspitzen gibt es bei Beziehungen von Usecases untereinander, z.B. Vererbung (geschlossen) und include/extend (offen)).

- Das Use-Case-Diagramm ist kein Flussdiagramm. Es werden also keine zeitlich/logischen Abfolgen modelliert (also nicht z.B. „erst Login, dann Suche als angemeldeter Nutzer, dann Ergebnis filtern“). Es werden nur Anwendungsfälle verknüpft, deren Szenarien ineinandergreifen. Flussdiagrammen werden in der UML mit dem Aktivitätsdiagramm modelliert.

</span>

### Anwendungsfalldiagramm für eine Mitfahr-App

Es sollen mithilfe eines UML-Anwendungsfalldiagramms die folgenden funktionalen Anforderungen an eine Mitfahr-App beschrieben werden:

Mithilfe dieser App soll jede/r die Möglichkeit haben, nach Mitfahrgelegenheiten zu suchen und sich diese anzeigen zu lassen. Das System "Mitfahr-App" soll daher die folgenden Anforderungen umsetzen:

- Alle Nutzenden (Gäste und angemeldete Benutzer*innen) sollen in der Lage sein, mithilfe einer Suchmaske Mitfahrangebote angezeigt zu bekommen.

- Gäste sollen in der Lage sein, sich am System über eine Login-Maske anzumelden.

- Für den Fall, dass die Gäste über noch kein Konto verfügen sollen Sie über die Neukunden-Registrierung ein eigenes Benutzerkonto erstellen können.

- Angemeldete Benutzer*innen können Anfragen an Anbieter*innen von Mitfahrgelegenheiten senden. Das beinhaltet immer, dass sie mit einer Captcha-Nachfrage bestätigen, kein Bot zu sein.

- Premium-Benutzer*innen (bspw. per kostenpflichtigem Abo) dürfen alles, was auch angemeldete Benutzer*innen dürfen.

- Premium-Benutzer*innen können Anzeigen für Mitfahrgelegenheiten im System aufgeben, um so nach Mitfahrenden zu suchen.

- Premium-Benutzer*innen können auf Anfragen von Mitfahrenden reagieren.

- Admins können alles, was auch angemeldete Benutzer*innen dürfen.

- Admins können zusätzlich noch Stammdaten bearbeiten. Hierfür müssen Sie ihre Identität nachweisen, in dem Sie ein Captcha lösen und sich mit einem zweiten Faktor authentifizieren, sofern sie das in dieser Session noch nicht gemacht haben.

- Diese Identitätsprüfung erweitert den Captcha-Anwendungsfall um die Abfrage eines zweiten Faktors (2FA).

- Premium-Nutzende können gemeinsam mit angemeldeten Benutzer*innen eine gegenseitige Bewertung erstellen.

- Die Anwendungsfälle Anfragen, Antworten, Anbieten und Stammdaten bearbeiten beinhalten immer, dass die Oberfläche gemäß der Einstellungen des Profils individualisiert werden kann (und entsprechende Einstellungen automatisch gespeichert werden).

<button onclick="toggleAnswer('uc1')">Antwort ein/ausblenden</button>

<span class="hidden-answer" id="uc1">

Eine Beispiellösung könnte etwa so aussehen:

![UML-Use-Case-Diagramm zur Mitfahr-App](plantuml/Mitfahr-App.png)

Hinweise:

- Ein häufiger Fehler sind die "Oder"-Beziehungen (Akteur*in A oder Akteur*in B kann XY machen): diese müssen immer mit Vererbung gelöst werden. Zwei eingehenden Kanten bei Usecases, die von Akteuren kommen, bedeuten "Und"! (siehe: Premium Benutzer*in UND angemeldete Benutzer*in können sich bewerten)

- Usecases sollten eigentlich aus sich heraus einen Nutzen für Benutzer*innen produzieren. Bei den Usecases "Nachweisen, kein Bot zu sein" und "Überprüfung der Identität ist das fraglich.

</span>
